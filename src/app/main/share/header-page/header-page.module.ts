import { NgModule } from '@angular/core';
import { FuseSharedModule } from '../../../../@fuse/shared.module';
import { MatIconModule, MatSlideToggleModule } from '@angular/material';
import { HeaderPageComponent } from './header-page.component';

@NgModule({
  declarations: [HeaderPageComponent],
  imports: [
    FuseSharedModule,
    MatIconModule,
    MatSlideToggleModule
  ],
  exports: [
    HeaderPageComponent
  ]
})
export class HeaderPageModule { }
