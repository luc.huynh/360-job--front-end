import { NgModule } from '@angular/core';
import { FuseSharedModule } from '../../../@fuse/shared.module';
import { HeaderPageModule } from './header-page/header-page.module';

@NgModule({
  declarations: [],
  imports: [
    FuseSharedModule,
    HeaderPageModule
  ]
})
export class ShareModule { }
