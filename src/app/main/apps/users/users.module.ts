import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FuseSharedModule } from '../../../../@fuse/shared.module';
import { UserListingComponent } from './user-listing/user-listing.component';
import { HeaderPageModule } from '../../share/header-page/header-page.module';
import {
  MatAutocompleteModule,
  MatButtonModule, MatCheckboxModule, MatChipsModule, MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule
} from '@angular/material';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {
    path: '**',
    component: UserListingComponent
  }
];

@NgModule({
  declarations: [
    UserListingComponent,
    UserComponent
  ],
  imports: [
    RouterModule.forChild(routes),

    FuseSharedModule,
    HeaderPageModule,
    MatFormFieldModule,
    MatTableModule,
    MatInputModule,
    MatPaginatorModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatOptionModule,
    MatSelectModule,
    MatChipsModule,
    MatAutocompleteModule
  ],
  entryComponents: [
    UserComponent
  ]
})
export class UsersModule {
}
