import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { FormControl } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { User } from '../../../../mock-data/user';
import { UserComponent } from '../user/user.component';

@Component({
  selector: 'app-user-listing',
  templateUrl: './user-listing.component.html',
  styleUrls: ['./user-listing.component.scss']
})
export class UserListingComponent implements OnInit {

  index = 0;

  displayedColumns: string[] = [
    'action',
    'select',
    'accounts',
    'email',
    'created',
    'firstName',
    'lastName',
    'telephonePrimary',
    'addressCountry',
    'lastLogin',
    'enabled'
  ];

  dataSource = new MatTableDataSource(USER_DATA);
  selection = new SelectionModel(true, []);

  accountsFilter = new FormControl('');
  emailFilter = new FormControl('');
  createdFilter = new FormControl('');
  firstNameFilter = new FormControl('');
  lastNameFilter = new FormControl('');
  telephonePrimaryFilter = new FormControl('');
  addressCountryFilter = new FormControl('');
  lastLoginFilter = new FormControl('');
  enabledFilter = new FormControl('');

  filterValues = {
    accounts: '',
    email: '',
    created: '',
    firstName: '',
    lastName: '',
    telephonePrimary: '',
    addressCountry: '',
    lastLogin: '',
    enabled: ''
  };

  animal: string;
  name: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog) {
    this.dataSource.filterPredicate = this.tableFilter();
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;

    this.initFilterTable();
  }

  public initFilterTable(): void {
    // this.accountsFilter.valueChanges
    //   .subscribe(
    //     data => {
    //       this.filterValues.accounts = data;
    //       this.dataSource.filter = JSON.stringify(this.filterValues);
    //     }
    //   );
    this.emailFilter.valueChanges
      .subscribe(
        data => {
          this.filterValues.email = data;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      );
    this.createdFilter.valueChanges
      .subscribe(
        data => {
          this.filterValues.created = data;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      );
    this.firstNameFilter.valueChanges
      .subscribe(
        data => {
          this.filterValues.firstName = data;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      );
    this.lastNameFilter.valueChanges
      .subscribe(
        data => {
          this.filterValues.lastName = data;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      );
    this.telephonePrimaryFilter.valueChanges
      .subscribe(
        data => {
          this.filterValues.telephonePrimary = data;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      );
    this.addressCountryFilter.valueChanges
      .subscribe(
        data => {
          this.filterValues.addressCountry = data;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      );
    this.lastLoginFilter.valueChanges
      .subscribe(
        data => {
          this.filterValues.lastLogin = data;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      );
    this.enabledFilter.valueChanges
      .subscribe(
        data => {
          this.filterValues.enabled = data;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      );
  }

  public tableFilter(): (data: any, filter: string) => boolean {
    return function (data, filter): boolean {
      const searchTerms = JSON.parse(filter);
      return data.email.toString().toLowerCase().indexOf(searchTerms.email) !== -1
        && data.created.toString().toLowerCase().indexOf(searchTerms.created) !== -1
        && data.firstName.toString().toLowerCase().indexOf(searchTerms.firstName) !== -1
        && data.lastName.toString().toLowerCase().indexOf(searchTerms.lastName) !== -1
        && data.telephonePrimary.toString().toLowerCase().indexOf(searchTerms.telephonePrimary) !== -1
        && data.addressCountry.toString().toLowerCase().indexOf(searchTerms.addressCountry) !== -1
        && data.lastLogin.toString().toLowerCase().indexOf(searchTerms.lastLogin) !== -1
        && data.enabled.toString().toLowerCase().indexOf(searchTerms.enabled) !== -1;
    };
  }

  public isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    // const numRows = this.paginator.pageSize;
    const numRows = this.dataSource.data.length;

    return numSelected === numRows;
  }

  public masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  // public masterToggle(): void {
  //   this.isAllSelected() ?
  //     this.selection.clear() : this.selectRows();
  // }
  //
  public selectRows(): void {
    for (let index = 0; index < this.dataSource.paginator.pageSize; index++) {
      this.selection.select(this.dataSource.data[index]);
      // this.selectionAmount = this.selection.selected.length;
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(UserComponent, {
      width: '1000px',
      data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
      console.log(result);
    });
  }

}

const USER_DATA: User[] = [
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: false
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: false
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  },
  {
    created: '1/23/2019 6:02',
    email: 'eightcap@gmail.com',
    firstName: 'Lynn',
    lastName: 'Rox',
    telephonePrimary: '123456',
    addressCountry: 'AU',
    lastLogin: '',
    enabled: true
  }
];
