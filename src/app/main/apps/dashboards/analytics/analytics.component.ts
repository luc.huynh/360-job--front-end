import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AnalyticsDashboardService } from 'app/main/apps/dashboards/analytics/analytics.service';
import { User } from '../../../../mock-data/user';
import { Deposit } from '../../../../mock-data/deposit';
import { Withdrawal } from '../../../../mock-data/withdrawal';
import { ChangeRequest } from '../../../../mock-data/change-request';

@Component({
  selector: 'analytics-dashboard',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})

export class AnalyticsDashboardComponent implements OnInit {
  widgets: any;
  widget5SelectedDay = 'today';
  usersLatestDisplayedColumns: string[] = ['created', 'firstName', 'lastName', 'telephonePrimary', 'addressCountry', 'lastLogin', 'enabled', 'action'];
  depositLatestDisplayedColumns: string[] = ['number', 'orderNumber', 'depositAmount', 'depositCurrency', 'settlementAmount', 'settlementCurrency', 'status', 'action'];
  withdrawalRequestDisplayColumns: string[] = ['unionPayCardNumber', 'unionPayClientName', 'unionPayBankName', 'unionPayBankAddress', 'unionPayBankTelephone', 'action'];
  changeRequestDisplayColumns: string[] = ['login', 'type', 'current', 'requested', 'balance', 'status', 'action'];

  changeRequest: ChangeRequest[] = [
    {
      email: '418488466@qq.com',
      login: '838823',
      type: 'Leverage',
      current: 100,
      requested: 500,
      balance: '$500.00',
      status: 'Approved',
      action: '-',
      created: 'March 11, 2019 12:46'
    },
    {
      email: '418488466@qq.com',
      login: '838823',
      type: 'Leverage',
      current: 100,
      requested: 500,
      balance: '$500.00',
      status: 'Pending Approval',
      action: '-',
      created: 'March 11, 2019 12:46'
    },
    {
      email: '418488466@qq.com',
      login: '838823',
      type: 'Leverage',
      current: 100,
      requested: 500,
      balance: '$500.00',
      status: 'Approved',
      action: '-',
      created: 'March 11, 2019 12:46'
    },
    {
      email: '418488466@qq.com',
      login: '838823',
      type: 'Leverage',
      current: 100,
      requested: 500,
      balance: '$500.00',
      status: 'Pending Approval',
      action: '-',
      created: 'March 11, 2019 12:46'
    },
    {
      email: '418488466@qq.com',
      login: '838823',
      type: 'Leverage',
      current: 100,
      requested: 500,
      balance: '$500.00',
      status: 'Pending Approval',
      action: '-',
      created: 'March 11, 2019 12:46'
    },
    {
      email: '418488466@qq.com',
      login: '838823',
      type: 'Leverage',
      current: 100,
      requested: 500,
      balance: '$500.00',
      status: 'Pending Approval',
      action: '-',
      created: 'March 11, 2019 12:46'
    },
    {
      email: '418488466@qq.com',
      login: '838823',
      type: 'Leverage',
      current: 100,
      requested: 500,
      balance: '$500.00',
      status: 'Pending Approval',
      action: '-',
      created: 'March 11, 2019 12:46'
    },
    {
      email: '418488466@qq.com',
      login: '838823',
      type: 'Leverage',
      current: 100,
      requested: 500,
      balance: '$500.00',
      status: 'Pending Approval',
      action: '-',
      created: 'March 11, 2019 12:46'
    }
  ];

  usersLatest: User[] = [
    {
      created: '1/23/2019 6:02',
      email: '418488466@qq.com',
      firstName: 'Lynn',
      lastName: 'Rox',
      telephonePrimary: '123456',
      addressCountry: 'AU',
      lastLogin: null,
      enabled: false
    },
    {
      created: '1/23/2019 6:02',
      email: '418488466@qq.com',
      firstName: 'Lynn',
      lastName: 'Rox',
      telephonePrimary: '123456',
      addressCountry: 'AU',
      lastLogin: null,
      enabled: true
    },
    {
      created: '1/23/2019 6:02',
      email: '418488466@qq.com',
      firstName: 'Lynn',
      lastName: 'Rox',
      telephonePrimary: '123456',
      addressCountry: 'AU',
      lastLogin: null,
      enabled: false
    },
    {
      created: '1/23/2019 6:02',
      email: '418488466@qq.com',
      firstName: 'Lynn',
      lastName: 'Rox',
      telephonePrimary: '123456',
      addressCountry: 'AU',
      lastLogin: null,
      enabled: true
    },
    {
      created: '1/23/2019 6:02',
      email: '418488466@qq.com',
      firstName: 'Lynn',
      lastName: 'Rox',
      telephonePrimary: '123456',
      addressCountry: 'AU',
      lastLogin: null,
      enabled: true
    },
    {
      created: '1/23/2019 6:02',
      email: '418488466@qq.com',
      firstName: 'Lynn',
      lastName: 'Rox',
      telephonePrimary: '123456',
      addressCountry: 'AU',
      lastLogin: null,
      enabled: true
    },
    {
      created: '1/23/2019 6:02',
      email: '418488466@qq.com',
      firstName: 'Lynn',
      lastName: 'Rox',
      telephonePrimary: '123456',
      addressCountry: 'AU',
      lastLogin: null,
      enabled: true
    },
    {
      created: '1/23/2019 6:02',
      email: '418488466@qq.com',
      firstName: 'Lynn',
      lastName: 'Rox',
      telephonePrimary: '123456',
      addressCountry: 'AU',
      lastLogin: null,
      enabled: true
    }
  ];

  withdrawalRequest: Withdrawal[] = [
    {
      code: '8d67130a21d72975c79f1aa1fa9ff328',
      type: 'union_pay',
      unionPayCardNumber: '6222021205009040621',
      unionPayClientName: '测试',
      unionPayBankName: '测试银行',
      unionPayBankAddress: '测试',
      unionPayBankTelephone: '5164855',
      bankAccountSortCodeBSB: null,
      bankAccountAccountNumber: null,
      bankAccountSwiftBICCode: null,
      bankAccountBankName: null,
      bankAccountBankAddress: null,
      bankAccountBankTelephone: null,
      skrill: null,
      amount: 'USD 11.00',
      email: '365208901@qq.com',
      tradingAccount: '821988',
      created: 'March 11,2019 02:55'
    },
    {
      code: '8d67130a21d72975c79f1aa1fa9ff328',
      type: 'union_pay',
      unionPayCardNumber: '6222021205009040621',
      unionPayClientName: '测试',
      unionPayBankName: '测试银行',
      unionPayBankAddress: '测试',
      unionPayBankTelephone: '5164855',
      bankAccountSortCodeBSB: null,
      bankAccountAccountNumber: null,
      bankAccountSwiftBICCode: null,
      bankAccountBankName: null,
      bankAccountBankAddress: null,
      bankAccountBankTelephone: null,
      skrill: null,
      amount: 'USD 11.00',
      email: '365208901@qq.com',
      tradingAccount: '821988',
      created: 'March 11,2019 02:55'
    },
    {
      code: '8d67130a21d72975c79f1aa1fa9ff328',
      type: 'union_pay',
      unionPayCardNumber: '6222021205009040621',
      unionPayClientName: '测试',
      unionPayBankName: '测试银行',
      unionPayBankAddress: '测试',
      unionPayBankTelephone: '5164855',
      bankAccountSortCodeBSB: null,
      bankAccountAccountNumber: null,
      bankAccountSwiftBICCode: null,
      bankAccountBankName: null,
      bankAccountBankAddress: null,
      bankAccountBankTelephone: null,
      skrill: null,
      amount: 'USD 11.00',
      email: '365208901@qq.com',
      tradingAccount: '821988',
      created: 'March 11,2019 02:55'
    },
    {
      code: '8d67130a21d72975c79f1aa1fa9ff328',
      type: 'union_pay',
      unionPayCardNumber: '6222021205009040621',
      unionPayClientName: '测试',
      unionPayBankName: '测试银行',
      unionPayBankAddress: '测试',
      unionPayBankTelephone: '5164855',
      bankAccountSortCodeBSB: null,
      bankAccountAccountNumber: null,
      bankAccountSwiftBICCode: null,
      bankAccountBankName: null,
      bankAccountBankAddress: null,
      bankAccountBankTelephone: null,
      skrill: null,
      amount: 'USD 11.00',
      email: '365208901@qq.com',
      tradingAccount: '821988',
      created: 'March 11,2019 02:55'
    },
    {
      code: '8d67130a21d72975c79f1aa1fa9ff328',
      type: 'union_pay',
      unionPayCardNumber: '6222021205009040621',
      unionPayClientName: '测试',
      unionPayBankName: '测试银行',
      unionPayBankAddress: '测试',
      unionPayBankTelephone: '5164855',
      bankAccountSortCodeBSB: null,
      bankAccountAccountNumber: null,
      bankAccountSwiftBICCode: null,
      bankAccountBankName: null,
      bankAccountBankAddress: null,
      bankAccountBankTelephone: null,
      skrill: null,
      amount: 'USD 11.00',
      email: '365208901@qq.com',
      tradingAccount: '821988',
      created: 'March 11,2019 02:55'
    },
    {
      code: '8d67130a21d72975c79f1aa1fa9ff328',
      type: 'union_pay',
      unionPayCardNumber: '6222021205009040621',
      unionPayClientName: '测试',
      unionPayBankName: '测试银行',
      unionPayBankAddress: '测试',
      unionPayBankTelephone: '5164855',
      bankAccountSortCodeBSB: null,
      bankAccountAccountNumber: null,
      bankAccountSwiftBICCode: null,
      bankAccountBankName: null,
      bankAccountBankAddress: null,
      bankAccountBankTelephone: null,
      skrill: null,
      amount: 'USD 11.00',
      email: '365208901@qq.com',
      tradingAccount: '821988',
      created: 'March 11,2019 02:55'
    },
    {
      code: '8d67130a21d72975c79f1aa1fa9ff328',
      type: 'union_pay',
      unionPayCardNumber: '6222021205009040621',
      unionPayClientName: '测试',
      unionPayBankName: '测试银行',
      unionPayBankAddress: '测试',
      unionPayBankTelephone: '5164855',
      bankAccountSortCodeBSB: null,
      bankAccountAccountNumber: null,
      bankAccountSwiftBICCode: null,
      bankAccountBankName: null,
      bankAccountBankAddress: null,
      bankAccountBankTelephone: null,
      skrill: null,
      amount: 'USD 11.00',
      email: '365208901@qq.com',
      tradingAccount: '821988',
      created: 'March 11,2019 02:55'
    },
    {
      code: '8d67130a21d72975c79f1aa1fa9ff328',
      type: 'union_pay',
      unionPayCardNumber: '6222021205009040621',
      unionPayClientName: '测试',
      unionPayBankName: '测试银行',
      unionPayBankAddress: '测试',
      unionPayBankTelephone: '5164855',
      bankAccountSortCodeBSB: null,
      bankAccountAccountNumber: null,
      bankAccountSwiftBICCode: null,
      bankAccountBankName: null,
      bankAccountBankAddress: null,
      bankAccountBankTelephone: null,
      skrill: null,
      amount: 'USD 11.00',
      email: '365208901@qq.com',
      tradingAccount: '821988',
      created: 'March 11,2019 02:55'
    }
  ];

  depositLatest: Deposit[] = [
    {
      number: 'L823238N1509942649',
      orderNumber: 7951629,
      clientEmail: '578925168@qq.com',
      depositAmount: 3000,
      depositCurrency: 'USD',
      settlementAmount: 19882.8,
      settlementCurrency: 'CNY',
      created: 'Mon, 06 Nov 2017 06:30:50',
      bankCard: null,
      bankCardId: null,
      status: 'Remote payment accepted'
    }, {
      number: 'L823238N1509942649',
      orderNumber: 7951629,
      clientEmail: '578925168@qq.com',
      depositAmount: 3000,
      depositCurrency: 'USD',
      settlementAmount: 19882.8,
      settlementCurrency: 'CNY',
      created: 'Mon, 06 Nov 2017 06:30:50',
      bankCard: null,
      bankCardId: null,
      status: 'Shipped'
    }, {
      number: 'L823238N1509942649',
      orderNumber: 7951629,
      clientEmail: '578925168@qq.com',
      depositAmount: 3000,
      depositCurrency: 'USD',
      settlementAmount: 19882.8,
      settlementCurrency: 'CNY',
      created: 'Mon, 06 Nov 2017 06:30:50',
      bankCard: null,
      bankCardId: null,
      status: 'Awaiting PayPal payment'
    }, {
      number: 'L823238N1509942649',
      orderNumber: 7951629,
      clientEmail: '578925168@qq.com',
      depositAmount: 3000,
      depositCurrency: 'USD',
      settlementAmount: 19882.8,
      settlementCurrency: 'CNY',
      created: 'Mon, 06 Nov 2017 06:30:50',
      bankCard: null,
      bankCardId: null,
      status: 'On pre-order (not paid)'
    }, {
      number: 'L823238N1509942649',
      orderNumber: 7951629,
      clientEmail: '578925168@qq.com',
      depositAmount: 3000,
      depositCurrency: 'USD',
      settlementAmount: 19882.8,
      settlementCurrency: 'CNY',
      created: 'Mon, 06 Nov 2017 06:30:50',
      bankCard: null,
      bankCardId: null,
      status: 'Preparing the order'
    }, {
      number: 'L823238N1509942649',
      orderNumber: 7951629,
      clientEmail: '578925168@qq.com',
      depositAmount: 3000,
      depositCurrency: 'USD',
      settlementAmount: 19882.8,
      settlementCurrency: 'CNY',
      created: 'Mon, 06 Nov 2017 06:30:50',
      bankCard: null,
      bankCardId: null,
      status: 'Delivered'
    }, {
      number: 'L823238N1509942649',
      orderNumber: 7951629,
      clientEmail: '578925168@qq.com',
      depositAmount: 3000,
      depositCurrency: 'USD',
      settlementAmount: 19882.8,
      settlementCurrency: 'CNY',
      created: 'Mon, 06 Nov 2017 06:30:50',
      bankCard: null,
      bankCardId: null,
      status: 'Payment error'
    }, {
      number: 'L823238N1509942649',
      orderNumber: 7951629,
      clientEmail: '578925168@qq.com',
      depositAmount: 3000,
      depositCurrency: 'USD',
      settlementAmount: 19882.8,
      settlementCurrency: 'CNY',
      created: 'Mon, 06 Nov 2017 06:30:50',
      bankCard: null,
      bankCardId: null,
      status: 'Payment accepted'
    }
  ];

  constructor(
    private _analyticsDashboardService: AnalyticsDashboardService
  ) {
    // Register the custom chart.js plugin
    this._registerCustomChartJSPlugin();
  }

  ngOnInit(): void {
    // Get the widgets from the service
    this.widgets = this._analyticsDashboardService.widgets;
  }

  /**
   * Register a custom plugin
   */
  private _registerCustomChartJSPlugin(): void {
    (<any>window).Chart.plugins.register({
      afterDatasetsDraw: function (chart, easing): any {
        // Only activate the plugin if it's made available
        // in the options
        if (
          !chart.options.plugins.xLabelsOnTop ||
          (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
        ) {
          return;
        }

        // To only draw at the end of animation, check for easing === 1
        const ctx = chart.ctx;

        chart.data.datasets.forEach(function (dataset, i): any {
          const meta = chart.getDatasetMeta(i);
          if (!meta.hidden) {
            meta.data.forEach(function (element, index): any {

              // Draw the text in black, with the specified font
              ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
              const fontSize = 13;
              const fontStyle = 'normal';
              const fontFamily = 'Roboto, Helvetica Neue, Arial';
              ctx.font = (<any>window).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

              // Just naively convert to string for now
              const dataString = dataset.data[index].toString() + 'k';

              // Make sure alignment settings are correct
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';
              const padding = 15;
              const startY = 24;
              const position = element.tooltipPosition();
              ctx.fillText(dataString, position.x, startY);

              ctx.save();

              ctx.beginPath();
              ctx.setLineDash([5, 3]);
              ctx.moveTo(position.x, startY + padding);
              ctx.lineTo(position.x, position.y - padding);
              ctx.strokeStyle = 'rgba(255,255,255,0.12)';
              ctx.stroke();

              ctx.restore();
            });
          }
        });
      }
    });
  }
}

