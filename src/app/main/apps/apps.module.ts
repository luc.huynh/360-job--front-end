import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatButtonModule, MatIconModule } from '@angular/material';

const routes = [
  {
    path: 'dashboards',
    loadChildren: './dashboards/analytics/analytics.module#AnalyticsDashboardModule'
  },
  {
    path: 'users',
    loadChildren: './users/users.module#UsersModule'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    MatButtonModule,
    MatIconModule
  ],
  declarations: []
})

export class AppsModule {
}
