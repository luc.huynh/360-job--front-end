import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
  {
    id: 'dashboards',
    title: 'Dashboards',
    type: 'item',
    icon: 'dashboard',
    url: '/dashboards'
  },
  {
    id: 'users',
    title: 'Users',
    type: 'collapsable',
    icon: 'people',
    children: [
      {
        id: 'account-users',
        title: 'Account Users',
        type: 'item',
        url: '/users'
      },
      {
        id: 'profiles',
        title: 'Profiles',
        type: 'item',
        url: '/profiles'
      }
    ]
  },
  {
    id: 'trading-accounts',
    title: 'Tradding Accounts',
    type: 'collapsable',
    icon: 'swap_horizontal_circle',
    children: [
      {
        id: 'accounts',
        title: 'Accounts',
        type: 'item',
        url: '/accounts'
      },
      {
        id: 'groups',
        title: 'Groups',
        type: 'item',
        url: '/groups'
      }
    ]
  },
  {
    id: 'fund-management',
    title: 'Fund Management',
    type: 'collapsable',
    icon: 'payment',
    children: [
      {
        id: 'deposits',
        title: 'Deposits',
        type: 'item',
        url: '/deposits'
      },
      {
        id: 'withdrawal',
        title: 'Withdrawal',
        type: 'item',
        url: '/withdrawal'
      },
      {
        id: 'funding-method',
        title: 'Funding Methods',
        type: 'item',
        url: '/funding-method'
      }
    ]
  },
  {
    id: 'requests',
    title: 'Requests',
    type: 'collapsable',
    icon: 'forum',
    children: [
      {
        id: 'leverage',
        title: 'Leverage',
        type: 'item',
        url: '/leverage'
      },
      {
        id: 'others',
        title: 'Others',
        type: 'item',
        url: '/other'
      }
    ]
  },
  {
    id: 'monitoring',
    title: 'Monitoring',
    type: 'item',
    icon: 'remove_from_queue',
    url: '/monitoring'
  },
  {
    id: 'special-features',
    title: 'Special Features',
    type: 'item',
    icon: 'stars',
    url: '/special-features'
  },
  {
    id: 'settings',
    title: 'Settings',
    type: 'item',
    icon: 'settings',
    url: '/settings'
  }
];
