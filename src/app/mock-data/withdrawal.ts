export interface Withdrawal {
    code: string;
    type: string;
    unionPayCardNumber: string;
    unionPayClientName: string;
    unionPayBankName: string;
    unionPayBankAddress: string;
    unionPayBankTelephone: string;
    bankAccountSortCodeBSB: string;
    bankAccountAccountNumber: string;
    bankAccountSwiftBICCode: string;
    bankAccountBankName: string;
    bankAccountBankAddress: string;
    bankAccountBankTelephone: string;
    skrill: string;
    amount: string;
    email: string;
    tradingAccount: string;
    created: string;
}
