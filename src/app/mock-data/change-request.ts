export interface ChangeRequest {
    email: string;
    login: string;
    type: string;
    current: number;
    requested: number;
    balance: string;
    status: string;
    action: string;
    created: string;
}
