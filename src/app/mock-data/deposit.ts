export interface Deposit {
    number: string;
    orderNumber: number;
    clientEmail: string;
    depositAmount: number;
    depositCurrency: string;
    settlementAmount: number;
    settlementCurrency: string;
    created: string;
    bankCard: string;
    bankCardId: string;
    status: string;
}
