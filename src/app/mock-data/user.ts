export interface User {
    created: string;
    email: string;
    firstName: string;
    lastName: string;
    telephonePrimary: string;
    addressCountry: string;
    lastLogin: string;
    enabled: boolean;
}
