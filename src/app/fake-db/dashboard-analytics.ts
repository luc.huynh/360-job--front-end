export class AnalyticsDashboardDb {
  public static widgets = {
    widget5: {
      chartType: 'line',
      datasets: {
        'year': [
          {
            label: 'Withdrawals',
            data: [59150, 66823, 79712, 34929, 89882, 29006, 23575, 57710, 9681, 55813, 23975, 47534],
            fill: 'start'

          },
          {
            label: 'Deposits',
            data:  [71860, 50195, 81174, 56475, 38744, 11569, 9652, 11047, 7209, 4767, 19510, 3256],
            fill: 'start'
          }
        ],
        'month': [
          {
            label: 'Withdrawals',
            data: [34509, 27364, 93133, 95967, 68419, 56678, 59409, 97023, 26343, 81043, 81637, 377],
            fill: 'start'

          },
          {
            label: 'Deposits',
            data: [36668, 11800, 38480, 24970, 66990, 39161, 8748, 23761, 6500, 85704, 70772, 99434],
            fill: 'start'
          }
        ],
        'yesterday': [
          {
            label: 'Withdrawals',
            data: [57902, 95177, 92450, 39264, 10226, 47058, 62211, 22160, 35267, 26220, 24165, 74456],
            fill: 'start'

          },
          {
            label: 'Deposits',
            data: [6871, 34342, 12399, 15667, 11312, 25937, 69425, 33243, 9089, 19243, 85633, 64786],
            fill: 'start'
          }
        ],
        'today': [
          {
            label: 'Withdrawals',
            data: [71544, 193, 45223, 13076, 31742, 68187, 47436, 7086, 58572, 20314, 44857, 68009],
            fill: 'start'
          },
          {
            label: 'Deposits',
            data: [99899, 57603, 3512, 3671, 78475, 48996, 19914, 75858, 15452, 43746, 68870, 64032],
            fill: 'start'

          }
        ]
      },
      labels: ['12am', '2am', '4am', '6am', '8am', '10am', '12pm', '2pm', '4pm', '6pm', '8pm', '10pm'],
      colors: [
        {
          borderColor: '#2dba69',
          backgroundColor: '#2dba69',
          pointBackgroundColor: '#2dba69',
          pointHoverBackgroundColor: '#2dba69',
          pointBorderColor: '#ffffff',
          pointHoverBorderColor: '#ffffff'
        },
        {
          borderColor: 'rgba(45, 186, 105, 0.5)',
          backgroundColor: 'rgba(45, 186, 105, 0.5)',
          pointBackgroundColor: 'rgba(45, 186, 105, 0.5)',
          pointHoverBackgroundColor: 'rgba(45, 186, 105, 0.5)',
          pointBorderColor: '#ffffff',
          pointHoverBorderColor: '#ffffff'
        }
      ],
      options: {
        spanGaps: false,
        legend: {
          display: false
        },
        maintainAspectRatio: false,
        tooltips: {
          position: 'nearest',
          mode: 'index',
          intersect: false
        },
        layout: {
          padding: {
            left: 24,
            right: 32
          }
        },
        elements: {
          point: {
            radius: 4,
            borderWidth: 2,
            hoverRadius: 4,
            hoverBorderWidth: 2
          }
        },
        scales: {
          xAxes: [
            {
              gridLines: {
                display: false
              },
              ticks: {
                fontColor: 'rgba(0,0,0,0.54)'
              }
            }
          ],
          yAxes: [
            {
              gridLines: {
                tickMarkLength: 20,
              },
              ticks: {
                stepSize: 10000
              }
            }
          ]
        },
        plugins: {
          filler: {
            propagate: false
          }
        }
      }
    }
  };
}
